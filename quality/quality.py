#!/usr/bin/env python3
"""
Take a Blender file and generate previews of the image at various samples settings

needs Python 3.6
"""

import glob
import os
import subprocess

samples = 250  # this should match the number of samples in your Blender file
chunks = 10  # in this case, each chunk will be 25 samples
blender_binary="/Applications/Blender/blender.app/Contents/MacOS/blender"
filename = "render.blend"
outdir = 'chunks_and_previews'
frames = [1, 7]  # list of frames you want to render
zpads = len(str(samples))
zpadc = len(str(chunks))

if not os.path.exists(outdir):
    os.makedirs(outdir)

os.chdir(outdir)

for frame in frames:
    # render chunks
    for chunk in range(1, chunks + 1):
        command = f"{blender_binary} -b ../{filename} -o frame_'#'_chunk_{str(chunk).zfill(zpadc)} -F EXR -f {frame} -- --cycles-resumable-num-chunks {chunks} --cycles-resumable-current-chunk {chunk}"
        subprocess.run(command, shell=True)

    # create preview images with different numbers of samples
    files = glob.glob(f"frame_{frame}_chunk_*.exr")
    for num_chunks in range(1, chunks + 1):
        files_to_combine = " ".join(files[:num_chunks])
        outfile = f"frame_{frame}_samples_{str((samples // chunks) * num_chunks).zfill(zpads)}"
        command = f"convert {files_to_combine} -evaluate-sequence mean -compress zip {outfile}.exr"
        subprocess.run(command, shell=True)
