# quality

Takes a Blender file and generates previews of the image at various samples settings, repeats the process for the list of frame numbers given.

---

##### How to get it working

The script should run on most modern *nix systems (macOS, Linux, Solaris etc) with Python 3.6 or newer. Set **samples** to the number of samples in the Blender file. Set **chunks** to the number of chunks to split **samples** into (this should divide nicely into **samples**). Then set **blender_binary** to something appropriate for your system, and **filename** to the Blender file to be rendered.

Run `./quality.py`.
