# render

#### [single-frame rendering](https://gitlab.com/skororu/scripts/tree/master/render/single_frame)

The idea of splitting the rendering of single frames into multiple chunks of work has a number of advantages.  For long running renders, this process makes rendering on shared compute facilities more practical, gives a degree of fault tolerance to crashes and power outages, and allows the workload to be trivially and efficiently parallelised.

The two scripts presented here, **distributed** and **single_computer**, both perform resumable single frame renders using Blender. The former implements a simple but effective distributed render. The latter performs the same basic operation, but on a single computer.

The **distributed** script uses GNU Parallel to perform the distributed render, which distributes the *Blender* file to be rendered to all the render nodes, collects rendered chunks, and tidies up after itself on the remote nodes. ImageMagick is then used to combine the images together to form the final image: [Example video](https://imgur.com/3THR6vO).

The **single_computer** script also uses GNU Parallel, though more for brevity than necessity: [Example video](https://imgur.com/CD5wiNu).

#### [animation rendering](https://gitlab.com/skororu/scripts/tree/master/render/animation)

Whilst it's fairly trivial to create a generic script to distribute ranges of frames to render on different computers, here's an example of how to do it using GNU Parallel, which at least handles the file transfers and tidies up after itself: [Example video](http://imgur.com/LbUSkqV).

---

##### How to get them working

These scripts should run on most modern *nix systems (macOS, Linux, Solaris etc). GNU Parallel is not available for Windows though it should still be possible to launch a distributed render from something *nix based (say a [Raspberry Pi](https://www.raspberrypi.org) or a Windows PC running a [VM](https://www.virtualbox.org)) using Windows render nodes.

To use these scripts you will need to have the render nodes on your network set up for passwordless ssh login; the **single_computer** script is the exception, which will run standalone. For configuration help, take a look at [Section (3) Configure SSH](https://gitlab.com/skororu/dtr/blob/master/readme.md#3-configure-ssh). Users with render nodes runnning [Cygwin/Windows](https://cygwin.com) should read the guidance [here](https://gitlab.com/skororu/dtr/blob/master/readme.md#windows) first, then refer to the Linux instructions [here](https://gitlab.com/skororu/dtr/blob/master/readme.md#32-linux). 

For all scripts, set the blender binary location correctly for your render nodes in the script. In the Blender file remember to pack external data and set threads to auto-detect.

For the distributed scripts **distributed** and **distributed_animation**, set the render nodes you want to use in `hosts.txt`, making sure they're prefixed with `1/` to indicate the number of CPU cores for each node. Since we allow Blender to use all available cores, we keep this set to 1 to ensure GNU Parallel only sends one render job at a time to each node.

For the single frame scripts **distributed** and **single_computer**, in the Blender file set the number of samples to be divisible by the number of chunks, and set the number of chunks in the script. E.g. if your goal is an 1800 sample image and you have 3 render nodes: set the Blender file to 1800 samples, and set the number of chunks in the script to 9.

Then run `./distributed`, `./single_computer` or `./distributed_animation`.
