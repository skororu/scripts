# Scripts

This repository contains a selection of unrelated shell scripts.  All examples are complete and will run standalone.

Documentation for the projects are contained in their respective directories.

|Directory|Function|
|:---:|:---:|
|[render](https://gitlab.com/skororu/scripts/tree/master/render)|Simple rendering examples with the [Resumable render implementation for Blender Cycles](https://developer.blender.org/rBf8b9f4e) using [GNU Parallel](https://www.gnu.org/software/parallel/) and [ImageMagick](https://imagemagick.org/script/convert.php)|
|[quality](https://gitlab.com/skororu/scripts/tree/master/quality)|Takes a Blender file and generates previews of the image at various samples settings with the [Resumable render implementation for Blender Cycles](https://developer.blender.org/rBf8b9f4e) and [ImageMagick](https://imagemagick.org/script/convert.php)|
